<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Helpers\AuditHelper;
use JWTAuth;
use App\{Warehouse, WarehouseProduct, Product, ProductMovement, EntityMasterdata, Company, BranchOffice, ProductTax};

class WarehouseController extends Controller
{
    public function __construct(){
       //$this->middleware('jwt.auth');
    }

    public function index(){
       $arr = [
            ['is_deleted', '<>', 1],
            ['is_active', 1],           
        ];         
       $warehouse = Warehouse::where($arr)->get(); 
       return $warehouse; 
    }

    public function indexDt() {
        $arr = [
            ['is_deleted', '<>', 1]            
        ];        
        $warehouse = Warehouse::where($arr)->get();            
        //AuditHelper::Audit($user->company_id, 'listar planes'); 
        return DataTables::collection($warehouse)->make(true);       
        //return $plans;
    }

    public function stockDt($company, $branch)
    {
        $cond = [
            ['company_id', $company],
            ['branch_id', $branch]
        ];

        $products = WarehouseProduct::where($cond)->with('product')->get();
        return DataTables::collection($products)->make(true); 
    }

    public function show($id){
        $entity = Warehouse::findOrFail($id);
        return $entity;
    }

    public function store(Request $request){

        $warehouse = Warehouse::create($request->all());

        return 1;

    }

    public function edit($id){
        $entity = Warehouse::findOrFail($id);
        return $entity;
    }

    public function update(Request $request, $id) {
        //$user = JWTAuth::parseToken()->authenticate();

        $warehouse = Warehouse::findOrFail($id);

        $warehouse->update($request->all());

        //AuditHelper::Audit($user->company_id, 'editar plan: ' . $plan->name);

        return 1;                            
    }

    public function destroy($id) { 
        //$user = JWTAuth::parseToken()->authenticate();
        $warehouse = Warehouse::findOrFail($id); 
        //AuditHelper::Audit($user->company_id, 'Eliminar plan: ' . $plan->name);
        $warehouse->is_active = 0;
        $warehouse->is_deleted = 1;
        $warehouse->update();
        //$plan->delete();               
        return 1;
    }

    public function products($warehouse, $company, $branch)
    {
        $cond = [
            ['warehouse_product.company_id', $company],
            ['warehouse_product.branch_id', $branch],
            ['warehouse_product.warehouse_id', $warehouse],
            ['product.is_active', 1]
        ];


        // $product = WarehouseProduct::where($arr)
        //             ->with('product')
        //             ->get();

        $products = WarehouseProduct::join('product','warehouse_product.product_id', '=', 'product.id')
                        ->select(\DB::raw('product.*'))
                        ->where($cond)
                        ->get();

        return $products;
    }

    public function storeEntryProduct(Request $request)
    {
        $company = $request->get('company');
        $branch  = $request->get('branch');
        $warehouse = $request->get('warehouse');
        $products = $request->get('products');
        $audit = $request->get('audit');
        $invoiceNumber = $request->get('invoiceNumber');
        $observation = $request->get('observation');
        $type = $request->get('type');
        $supplier = $request->get('supplier');

        foreach ($products as $pro) {

            $pro = json_decode($pro);
        
            $cond = [
                ['company_id', $company],
                ['branch_id', $branch],
                ['warehouse_id', $warehouse],
                ['product_id', $pro->product->product_id]
            ];

            
            $product = WarehouseProduct::where($cond)->first();

            if ($product) {
                $target_amount = $product->quantity;
                $product->quantity = $product->quantity + $pro->transferAmount;
            } else {
                $target_amount = $pro->currentAmount;

                $product = new WarehouseProduct;
                $product->company_id = $company;
                $product->branch_id = $branch;
                $product->warehouse_id = $warehouse;
                $product->product_id = $pro->product->product_id;
                $product->quantity = $pro->transferAmount;
            }
            
            $product->save();

            /**
             * Get Product by Audit o Update Precio Product
             */
            
            if ($audit == 1) {
                $pd = Product::findOrFail($pro->product->product_id);
            } else {
                Product::where('id', $pro->product->product_id)
                    ->update([
                        "unit_price" => $pro->salePrice,
                        "unit_cost" => $pro->priceCost,
                        "expired_date" => date("Y-m-d H:i:s", strtotime($pro->product->expiration_date))
                    ]);
            }
            
            /**
             * Creation of data for the movement record of the product
             * @var ProductMovements $pm
             */

            ProductMovement::create([
                "company_id" => $company,
                "branch_origin" => $branch,
                "warehouse_origin" => $warehouse,
                "destination_branch" => $branch,
                "destination_store" => $warehouse,
                "product_id" => $pro->product->product_id,
                "unit_cost" => ($audit == 1) ? $pd->unit_price : $pro->priceCost,
                "average_cost" => ($audit == 1) ? 0 : $pro->average_cost,
                "aliquot" => (empty($pro->aliquot)) ? '0.1' : $pro->aliquot->aliquot,
                "quantity_origin" => $pro->currentAmount,
                "target_amount" => $target_amount,
                "amount_send" => $pro->transferAmount,
                "current_origin_quantity" => $product->quantity,
                "current_destination_quantity" => $product->quantity,
                "supplier"  => ($audit == 1) ? 0 : $supplier,
                "sppay" => ($audit == 1) ? 0 : $pro->sppay,
                "invoiceNumber" => $invoiceNumber,
                "iva" => ($audit == 1) ? 0 : $pro->iva,
                "invoiceAmount" => ($audit == 1) ? $pd->unit_price * $pro->transferAmount : $pro->product->invoiceAmount,
                "observation"   => $observation,
                "created_at" => date('Y-m-d'),
                "expiration_date" => ($audit == 1) ? date("Y-m-d H:i:s") : date("Y-m-d H:i:s", strtotime($pro->product->expiration_date)),
                "types" => $type
            ]);
        }
        return 1;
    }

    public function updateEntryProduct(Request $request)
    {
        $company = $request->get('company');
        $branch  = $request->get('branch');
        $warehouse = $request->get('warehouse');
        $products = $request->get('products');
        $audit = $request->get('audit');
        $invoiceNumber = $request->get('invoiceNumber');
        $observation = $request->get('observation');
        $type = $request->get('type');
        $supplier = $request->get('supplier');

        foreach ($products as $pro) {

            $pro = json_decode($pro);

            $cond = [
                ['company_id', $company],
                ['branch_id', $branch],
                ['warehouse_id', $warehouse],
                ['product_id', $pro->product->product_id]
            ];
            
            if ($pro->modification) {
                $priceCost = 0;
                $product = WarehouseProduct::where($cond)->first();

                if ($pro->transferAmount != $pro->lastTransfer) {
                    $product->quantity = $product->quantity - $pro->lastTransfer;
                    $product->quantity = $product->quantity + $pro->transferAmount;
                    $product->save();
                }

                if ($pro->invoiceAmount != $pro->lastInvoiceAmount) {
                    
                }

                ProductMovement::where('id', $pro->record_id)
                    ->update([
                        "unit_cost" => $pro->priceCost,
                        "aliquot" => $pro->aliquot->aliquot,
                        "amount_send" => $pro->transferAmount,
                        "current_origin_quantity" => $product->quantity,
                        "current_destination_quantity" => $product->quantity,
                        "supplier"  => $supplier,
                        "sppay" => $pro->sppay,
                        "invoiceNumber" => $invoiceNumber,
                        "iva" => $pro->iva,
                        "invoiceAmount" => $pro->product->invoiceAmount,
                        "observation"   => $observation,
                        "created_at" => date('Y-m-d'),
                        "expiration_date" => date("Y-m-d H:i:s", strtotime($pro->product->expiration_date)),
                        "types" => $type
                    ]);
            } else {                
                $product = WarehouseProduct::where($cond)->first();

                if ($product) {
                    $target_amount = $product->quantity;
                    $product->quantity = $product->quantity + $pro->transferAmount;
                } else {
                    $target_amount = $pro->currentAmount;

                    $product = new WarehouseProduct;
                    $product->company_id = $company;
                    $product->branch_id = $branch;
                    $product->warehouse_id = $warehouse;
                    $product->product_id = $pro->product->product_id;
                    $product->quantity = $pro->transferAmount;
                }
                
                $product->save();

                /**
                 * Get Product by Audit o Update Precio Product
                 */
                
                Product::where('id', $pro->product->product_id)
                    ->update([
                        "unit_price" => $pro->salePrice,
                        "unit_cost" => $pro->priceCost,
                        "expired_date" => date("Y-m-d H:i:s", strtotime($pro->product->expiration_date))
                    ]);
               
                
                /**
                 * Creation of data for the movement record of the product
                 * @var ProductMovements $pm
                 */

                ProductMovement::create([
                    "company_id" => $company,
                    "branch_origin" => $branch,
                    "warehouse_origin" => $warehouse,
                    "destination_branch" => $branch,
                    "destination_store" => $warehouse,
                    "product_id" => $pro->product->product_id,
                    "unit_cost" => $pro->priceCost,
                    "average_cost" => $pro->average_cost,
                    "aliquot" => (empty($pro->aliquot)) ? '0.1' : $pro->aliquot->aliquot,
                    "quantity_origin" => $pro->currentAmount,
                    "target_amount" => $target_amount,
                    "amount_send" => $pro->transferAmount,
                    "current_origin_quantity" => $product->quantity,
                    "current_destination_quantity" => $product->quantity,
                    "supplier"  => $supplier,
                    "sppay" => $pro->sppay,
                    "invoiceNumber" => $invoiceNumber,
                    "iva" => $pro->iva,
                    "invoiceAmount" => $pro->product->invoiceAmount,
                    "observation"   => $observation,
                    "created_at" => date('Y-m-d'),
                    "expiration_date" => date("Y-m-d H:i:s", strtotime($pro->product->expiration_date)),
                    "types" => $type
                ]);
            }
        }
        return 1;
    }

    public function storeEntryTransfer(Request $request)
    {
        $company     = $request->get('company');
        $origin      = $request->get('origin');
        $destination = $request->get('destination');
        $doc_num     = $request->get('document_num');
        $products    = $request->get('products');
        $branch_origin = $request->get('branch_origin');
        $destination_branch = $request->get('destination_branch');

        foreach ($products as $pro) {
            $pro = json_decode($pro);
            
            $condOrigin = [
                ['company_id', $company],
                ['branch_id', $branch_origin],
                ['warehouse_id', $origin],
                ['product_id', $pro->product->product_id]
            ];

            $wo = WarehouseProduct::where($condOrigin)->first();
            $wo->quantity = $pro->currentAmount - $pro->transferAmount;
            $wo->save();

            /**
             * Creation of data for the movement record of the product
             * @var ProductMovements $pm
             */
            
            ProductMovement::create([
                "company_id" => $company,
                "branch_origin" => $branch_origin,
                "warehouse_origin" => $origin,
                "destination_branch" => $destination_branch,
                "destination_store" => $destination,
                "product_id" => $pro->product->product_id,
                "quantity_origin" => $pro->currentAmount,
                "target_amount" => 0,
                "amount_send" => $pro->transferAmount,
                "current_origin_quantity" => $wo->quantity,
                "current_destination_quantity" => 0,
                "document_number" => $doc_num,
                "created_at" => date('Y-m-d'),
                "types" => 7 
            ]);
            
        }

        return 1;
    }

    public function storeEntryOutput(Request $request)
    {
        $cond = [
            ['company_id', $request->get('company')],
            ['branch_id', $request->get('branch')],
            ['warehouse_id', $request->get('warehouse')],
            ['product_id', $request->get('product')]
        ];

        $wp = WarehouseProduct::where($cond)->first();

        $previousAmount = $wp->quantity;

        $wp->quantity = $wp->quantity - $request->get('quantity');

        $wp->save();    
        
        $product = Product::where('id',$request->get('product'))
                ->select('unit_price')
                ->first(); 

        /* Creation of data for the movement record of the product
         * @var ProductMovements
         */
        
        ProductMovement::create([
            "company_id" => $request->get('company'),
            "branch_origin" => $request->get('branch'),
            "warehouse_origin" => $request->get('warehouse'),
            "destination_branch" => $request->get('branch'),
            "destination_store" => $request->get('warehouse'),
            "product_id" => $request->get('product'),
            "quantity_origin" => $previousAmount,
            "amount_send" => $request->get('quantity'),
            "current_origin_quantity" => $wp->quantity,
            "invoiceAmount" => $request->get('quantity') * $product->unit_price,
            "observation" => $request->get('observation'),
            "created_at" => date('Y-m-d'),
            "types" => $request->get('type'),
            "document_number" => $request->get('document_number')
        ]);

        return 1;
    }

    public function getWarehouseProduct($company, $warehouse, $product, $branch)
    {
        $cond = [
            ['company_id', $company],
            ['branch_id', $branch],
            ['warehouse_id', $warehouse],
            ['product_id', $product]
        ];

        $wp = WarehouseProduct::where($cond)->with('product')->first();

        return (empty($wp)) ? 0 : $wp;
    }

    /**
     * [getWP Metodo encargado de obtener productos para alimentar agenda medica]
     * @param  [int] $company [Compañia asociada]
     * @param  [int] $branch  [description]
     * @return [object]          [WarehouseProduct]
     */
    public function getWP($company, $branch)
    {
        $cond = [
            ['warehouse_product.company_id', $company],
            ['warehouse_product.branch_id', $branch]
        ];

        $wp = WarehouseProduct::join("product", "warehouse_product.product_id","=","product.id")
                ->where($cond)
                ->select(\DB::raw("product.id, warehouse_product.quantity,product.unit_price, product.name, product.generic"))
                ->get();

        return $wp;
    }

    public function getTransfer($doc_num)
    {
        return ProductMovement::where('document_number', $doc_num)
                                ->with('warehouse_origin', 'product', 'supplier')
                                ->get();
    }

    public function acceptTransfer($doc_num)
    {
        $pms = ProductMovement::where('document_number', $doc_num)
                                ->get();
        foreach ($pms as $pm) {
            $branch_destination = $pm->destination_branch;
            
            $cond = [
                    ['company_id', $pm->company_id],
                    ['branch_id', $pm->destination_branch],
                    ['warehouse_id', $pm->destination_store],
                    ['product_id', $pm->product_id]
                ];

            $wd = WarehouseProduct::where($cond)->first();

            if ($wd) {
                $previousAmount = $wd->quantity;
                $wd->quantity = $wd->quantity + $pm->amount_send;
            } else {
                $previousAmount = 0;
                $wd =  new WarehouseProduct;
                $wd->company_id = $pm->company_id;
                $wd->warehouse_id = $pm->destination_store;
                $wd->product_id = $pm->product_id;
                $wd->quantity = $pm->amount_send;   
                $wd->branch_id = $pm->destination_branch;
            } 
            
            $wd->save();

            $pm->types = 3;
            $pm->target_amount = $previousAmount;
            $pm->current_destination_quantity = $wd->quantity;
            $pm->reception_date = date('Y-m-d H:i:s');

            $pm->save(); 
        }
        return 1;
    }

    public function warehouseTransfers($company, $warehouse)
    {
        $cond = [
            ['company_id', $company],
            ['warehouse_id', $warehouse]
        ];   

        return WarehouseProduct::where($cond)->with('product')->get();
    }

    public function indexMov($type)
    {
        if ($type == 'output') {
            $data = ProductMovement::whereIn('types', [2,4,5,6,9])->with('warehouse_origin', 'destination_store', 'product','supplier')->get();
        } else{
            $cond = ($type == 1) ? [1, 8] : [$type];
            $data = ProductMovement::whereIn('types', $cond)->with('warehouse_origin', 'destination_store', 'product','supplier')->get();
        }
        

        return DataTables::collection($data)->make(true); 
    }

    public function getDocumentProducts($type)
    {
        $data = '';
        switch ($type) {
            case '1':
                $data = ProductMovement::join('supplier','product_movements.supplier', '=', 'supplier.id')
                            ->select(\DB::raw('supplier.social_reason as supplier, product_movements.invoiceNumber, product_movements.created_at, SUM(product_movements.invoiceAmount) as invoiceAmount, SUM(product_movements.iva) as iva, (SUM(product_movements.invoiceAmount) - SUM(product_movements.sppay)) as sppay, SUM(product_movements.sppay) as rsp , product_movements.types, (SUM(product_movements.iva)+SUM(product_movements.invoiceAmount)) as invoiceTotal'))
                            ->groupBy('product_movements.invoiceNumber', 'product_movements.created_at', 'product_movements.types', 'supplier.social_reason')
                            ->orderBy('product_movements.invoiceNumber', 'ASC')
                            ->get();
                break;
        }

        return DataTables::collection($data)->make(true);
    }

    public function getProducts($invoice)
    {
        // return ProductMovement::join('product','product_movements.product_id', '=', 'product.id')
        //                 ->join('supplier', 'product_movements.supplier', '=', 'supplier.id')
        //                 ->select(\DB::raw('product.id as product_id, product_movements.id, product.name, product.description, product.unit_price, product.unit_cost, product.expired_date as expiration_date, supplier.*, product_movements.amount_send, product_movements.average_cost, product_movements.aliquot, product_movements.invoiceAmount, product_movements.iva, product_movements.quantity_origin, product_movements.amount_send'))
        //                 ->where('invoiceNumber', $invoice)
        //                 ->get();
        return ProductMovement::where('invoiceNumber', $invoice)
                        ->with('supplier', 'product', 'warehouse_origin', 'branch_origin')
                        ->get();
    }

    public function getCorrelativeTransfer($company, $branch)
    {
        $cond = [
            ['company_id', $company],
            ['branch_origin', $branch]
        ];

        $doc = ProductMovement::select('document_number')
                    ->where($cond)
                    ->whereIn('types', [3,7])
                    ->orderby('document_number', 'DESC')
                    ->first();
        if ($doc) {
            $doc = explode('-', $doc->document_number);
            $doc = $doc[0] . '-' . str_pad($doc[1]+1, 6, '0', STR_PAD_LEFT);
        } else {
            $doc = 'PTBS-' . str_pad(1, 6, '0', STR_PAD_LEFT);
        }

        return $doc;
    }

    public function typeReports()
    {
        $cond = [
            ['entity_id', 28],
            ['is_active', 1]
        ];

        return EntityMasterdata::where($cond)->get();
    }

    public function productByExpirationDate($company, $branch, $startDate, $endDate)
    {
        $cond = [
            ['product_movements.branch_origin', $branch],
            ['product_movements.expiration_date','>=',date('Y-m-d', strtotime($startDate))],
            ['product_movements.expiration_date','<=',date('Y-m-d', strtotime($endDate))]
        ];
        

        $company = Company::where('id',$company)->select('ruc', 'name', 'comercial_name', 'logo', 'address')->first();
        $branch = BranchOffice::where('id', $branch)->select('name', 'address')->first();
        $products = ProductMovement::join('product','product_movements.product_id', '=', 'product.id')
                        ->join('supplier', 'product_movements.supplier', '=', 'supplier.id')
                        ->select(\DB::raw('product_movements.amount_send, product_movements.created_at, product_movements.expiration_date, product.name, product.principal_code, product.laboratory, product.generic, supplier.comercial_name as supplier'))
                        ->where($cond)
                        ->get();
        $range = ["startDate" => $startDate, "endDate" =>$endDate];

        return json_encode([
                    "range" => $range,
                    "company" => $company,
                    "branch" => $branch,
                    "products" => $products
                ]);
    }

    public function productBranch($company, $branchId)
    {
        
        $company = Company::where('id',$company)->select('ruc', 'name', 'comercial_name', 'logo', 'address')->first();
        $branch = BranchOffice::where('id', $branchId)->select('name', 'address')->first();
        
        $products = ProductMovement::from('product_movements as a')
            ->whereIn('a.types', [1,3])
            ->where(function ($query) use ($branchId) {
              $query = $query->orWhere('a.branch_origin','like',"%$branchId%");
              $query = $query->orWhere('a.destination_branch','like',"%$branchId%");
            })
            ->with('product', 'supplier','branch_origin')
            ->get();

        foreach ($products as $key => $pro) {
            if ($pro->branch_origin  == $branchId and $pro->types == 3) {
                unset($products[$key]);
            }
        }

        return json_encode([
                    "company" => $company,
                    "branch" => $branch,
                    "products" => $products
                ]);
    }

    public function getCorrelativeAuditEntry($company, $branch)
    {
        $cond = [
            ['company_id', $company],
            ['branch_origin', $branch],
            ['types', 8]
        ];

        $doc = ProductMovement::select('invoiceNumber')
                    ->where($cond)
                    ->orderby('invoiceNumber', 'DESC')
                    ->first();
        if ($doc) {
            $doc = explode('-', $doc->invoiceNumber);
            $doc = $doc[0] . '-' . str_pad($doc[1]+1, 6, '0', STR_PAD_LEFT);
        } else {
            $doc = 'EBIA-' . str_pad(1, 6, '0', STR_PAD_LEFT);
        }

        return $doc;
    }

    public function getCorrelativeExitProduct($company, $branch, $type)
    {
        $cond = [
            ['company_id', $company],
            ['branch_origin', $branch],
            ['types', $type]
        ];

        $doc = ProductMovement::select('document_number')
                    ->where($cond)
                    ->orderby('document_number', 'DESC')
                    ->first();
        if ($doc) {
            $doc = explode('-', $doc->document_number);
            $doc = $doc[0] . '-' . str_pad($doc[1]+1, 6, '0', STR_PAD_LEFT);
        } else {
            switch ($type) {
                case '4':
                    $doc = 'EDP-' . str_pad(1, 6, '0', STR_PAD_LEFT);
                    break;
                
                case '5':
                    $doc = 'DNP-' . str_pad(1, 6, '0', STR_PAD_LEFT);
                    break;

                case '6':
                    $doc = 'DVP-' . str_pad(1, 6, '0', STR_PAD_LEFT);
                    break;

                case '9':
                    $doc = 'EIA-' . str_pad(1, 6, '0', STR_PAD_LEFT);
                    break;
            }
        }

        return $doc;
    }

    public function averageCostProduct($company, $branch, $product)
    {
        $cond = [
            ['company_id', $company],
            ['branch_origin', $branch],
            ['product_id', $product]
        ];

        $data = ProductMovement::where($cond)
                        ->select(\DB::raw('SUM(invoiceAmount) as inventary_cost, SUM(amount_send) as quantity'))
                        ->get();
        
        return json_encode([
                'inventary_cost' => number_format($data[0]['inventary_cost'], 2, '.', ''), 
                'quantity' => $data[0]['quantity']
        ]);   
    }

    public function stock($companyId, $branchId)
    {
        $company = Company::where('id',$companyId)->select('ruc', 'name', 'comercial_name', 'logo', 'address')->first();
        $branch = BranchOffice::where('id', $branchId)->select('name', 'address')->first();
           
        $cond = [
            ['warehouse_product.company_id', $companyId],
            ['warehouse_product.branch_id', $branchId]
        ];

        $products = WarehouseProduct::join('product','warehouse_product.product_id', '=', 'product.id')
                        ->select(\DB::raw('product.name, product.principal_code, product.generic, product.unit_price, product.is_active, warehouse_product.quantity'))
                        ->where($cond)
                        ->orderBy('product.principal_code','asc')
                        ->get();

        return json_encode([
                    "company" => $company,
                    "branch" => $branch,
                    "products" => $products
                ]);
    }
}
